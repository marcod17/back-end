<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <tr>
            <th>Title</th>
            <th>Abstract</th>
            @foreach($books as $book)
                <tr><td>{{ $book->title }}</td><td>{{ $book->abstract }}</td></tr>
            @endforeach
        </tr>
    </table>
</body>
</html>