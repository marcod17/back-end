<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Str; // importare l'oggetto per le stringhe random

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // metodo che viene chiamato quando si esegue php artisan db:seed
        // $this->call(UsersTableSeeder::class);
        // $numbers = range(1, 100);
        // shuffle($numbers);
        // decommenta per cancellare le tabelle
        // DB::table('authors')->delete();
        // DB::table('books')->delete();
        $faker = Faker::create();
        for ($i=0;$i<100;$i++){
            DB::table('customers')->insert(
                [
                'name' => $faker->name,
                'city' => $faker->city,
                'email' => $faker->safeEmail,
                'phone' => $faker->numberBetween($min = 100000000, $max = 9999999999),
                'age' => $faker->numberBetween($min = 18, $max = 99)
            ]);
        };
        //     DB::table('books')->insert([
        //         'title' => Str::random(20),
        //         'abstract' => Str::random(50),
        //         'description' => Str::random(50),
        //         'ean' => rand(1,9999999999999),
        //         'pages' => rand(1,3000),
        //     ]);
        //     // DB::table('books')->delete();
            
        //     DB::table('authors')->insert([
        //         'lastname' => Str::random(15),
        //         'firstname' => Str::random(12),
        //         'ranking' => $numbers[$i],
        //     ]);
        //     // DB::table('authors')->delete();
        // }
        // il risultato saranno 200 libri con stringhe e numeri random
    }
}
