<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use app\Customer;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'city' => $faker->city,
        'email' => $faker->safeEmail,
        'phone' => $faker->numberBetween($min = 100000000, $max = 9999999999),
        'age' => $faker->numberBetween($min = 18, $max = 99)
    ];
});
