<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAuthorIdToBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            // aggiungere author_id, specificare che può essere null, per non creare problemi con quelli già esistenti
            $table->unsignedBigInteger('author_id')->nullable();
            // si potrebbe già creare la relazione, ma per ora non c'è vincolo su database!
            // aggiungiamolo:
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');
            // così abbiamo legato il campo author_id al campo id di authors
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table) {
            // è buona pratica definire sempre la down prima di eseguire
            // prima occorre eliminare la foreign key
            $table->dropForeign('books_author_id_foreign');
            $table->dropColumn('author_id');
        });
    }
}
