<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * 1 Creazione -> POST /books
 * 2 Lettura tutti i libri -> GET /books
 * 3 Lettura di un singolo libro -> GET /books/{id}
 * 4 Modifica -> PUT /books/{id}
 * 5 Eliminazione -> DELETE /books/{id}
 */
 
//PREFISSO: /api/
//ESEMPIO: http://localhost:8000/api/books

 Route::post('/books', 'BooksController@store'); // 1
 Route::get('/books', 'BooksController@getAll'); // 2
 Route::get('/books/{id}', 'BooksController@get'); // 3
 Route::put('/books/{id}', 'BooksController@update'); // 4
 Route::delete('/books/{id}', 'BooksController@delete'); // 5
 Route::patch('/books/{id}', 'BooksController@patch');

 Route::post('/authors', 'AuthorsController@store'); // 1
 Route::get('/authors', 'AuthorsController@getAll'); // 2
 Route::get('/authors/{id}', 'AuthorsController@get'); // 3
 Route::put('/authors/{id}', 'AuthorsController@update'); // 4
 Route::delete('/authors/{id}', 'AuthorsController@delete'); // 5
 Route::patch('/authors/{id}', 'AuthorsController@patch');

 Route::get('/authors/{id}/books', 'AuthorsController@getAuthorBooks');
// rotta di tipo get per mostrare tutti i libri di un determinato autore
// si accede con autore/chiave/libro


Route::get('/customers', 'CustomersController@getAll');
Route::get('/customers/{id}', 'CustomersController@get');