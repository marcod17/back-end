<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    $current_datetime = date('d/m/Y H:i:s');
    $html_content = '<ul><li>item1</li><li>item2</li></ul>';
    return view('welcome', [
        'current_datetime' => $current_datetime,
        'html_content' => $html_content
        // solitamente si mantiene lo stesso standard per variabili e chiave 
    ]);
});
// stiamo chiendendo di richiamare una vista chiamata welcome dentro resources/views
// sono dei template html richiamabili

Route::get('/home', 'WebController@home');