<?php

namespace App\Http\Middleware;

use Closure;

class CORS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return response('TEST');
        $response = $next($request);
        // questa si tratta di una closure, ovvero usare una funzione come se fosse una variabile
        // $next praticamente sarebbe una variabile di TIPO funzione
        // laravel inietta dentro la variabile next la funzione che contiene l'applicativo stesso:
        //  richiesta -> rotta -> risposta -> ritorno al cliente
        // $next mette la risposta dentro una variabile, $response, al fine di renderla modificabile

        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', '*');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With, Application');
        
        return $response;
    }
}
