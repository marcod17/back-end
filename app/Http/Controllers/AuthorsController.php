<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use Illuminate\Support\Facades\Validator;

class AuthorsController extends Controller
{
    public function store(Request $request) {

        $validator = validator::make($request->all(),[
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'ranking' => 'required|integer|max:5',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()    
            ], 400);
        };

        $author = new Author();
        $author->lastname = $request->input('lastname');
        $author->firstname = $request->input('firstname');
        $author->ranking = $request->input('ranking');
        try {
            $author->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
        
        return $author;
    }

    public function getAll(Request $request) {
        // $author = Author::get(); // SELECT * FROM books
        // con la whit invece sarebbe: 
        // $author = Author::with('books')->get();
        // esempio solo un campo
        $author = Author::with(['books' => function($query){
            $query->select('author_id','title');
            // occorre chiamarci la funzione dentro quella query
        }])->get();
        return $author;
    }

    public function get(Request $request, $id) {
        //chiamata a database con where id=$id
        //SELECT * FROM users WHERE id=1
        // $author = Author::findOrFail($id);
        $author = Author::with('books')->findOrFail($id);
        // a differenza della funzione generica qui mostra anche i dati dell'autore
        return $author;
    }

    public function update(Request $request, $id) {
        $author = Author::findOrFail($id);
        $author->lastname = $request->input('lastname');
        $author->firstname = $request->input('firstname');
        $author->ranking = $request->input('ranking');
        try {
            $author->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
        return $author;
    }

    public function delete(Request $request, $id) {
        $author = Author::findOrFail($id);
        $author->delete();
        return response()->json(null, 204);
    }

    public function patch(Request $request, $id) {
        
    }

    public function getAuthorBooks(Request $request, $id){
        $author = Author::findOrFail($id);
        // andiamo ad accedere dentro all'autore attraverso il suo id
        return $author->books; // select authors from books where authors id = 2
    }
}
