<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function getAll(Request $request){
        $customers = Customer::get();
        return $customers;
    }

    public function get(Request $request, $id){
        $customer = Customer::findOrFail($id);
        return $customer;
    }
}
