<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Validator;

class BooksController extends Controller
{
    public function store(Request $request) {

        $validator = validator::make($request->all(),[
            'title' => 'required|max:200',
            'abstract' => 'required|max:1000',
            'description' => 'required',
            'ean' => 'required|max:13|digits:13',
            'pages' => 'required|integer|max:32767',
            // al seguito dell'impostazione della tabella, e dei modelli passiamo ad aggiungere l'id dell'autore
            'author_id' => 'required|integer|exists:authors,id'
            // necessario per risparmiare chiamate fallite ma anche errori gestendoli in precedenza, evitando di intasare i log
            // significa vai a cercare una riga nella tabella authors dove il campo id è uguale a questo author_id
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()    
            ], 400);
        };

        $book = new Book();
        $book->title = $request->input('title');
        $book->abstract = $request->input('abstract');
        $book->description = $request->input('description');
        $book->ean= $request->input('ean');
        $book->pages = $request->input('pages');
        // al seguito dell'impostazione della tabella, e dei modelli passiamo ad aggiungere l'id dell'autore
        // metodo facile (non elegante!)
        $book->author_id = $request->input('author_id');
        try {
            $book->save();
        } catch(\Exception $e) {
            // si mette \Exception con lo slash davanti perchè è una classe nativa di php, e gli specifichiamo di cercarla li al posto che nel namescape
            return response()->json(['error' => $e->getMessage()], 400);
            // gestione errore foreign key dell'author
            // gestiamo l'errore bloccante di tipo 500 (dello sviluppatore), inviando come risposta un JSON dell'errore e creando un errore di tipo 400
            // oltretutto nel messaggio non appaiono troppe informazioni riguardando la struttura della tabella
        }
        
        return $book;  

    }

    public function getAll(Request $request) {
        // $books = Book::get(); // SELECT * FROM books
        $books = Book::with('author')->get();
        // mostra l'intero oggetto correlato
        return $books;
    }

    public function get(Request $request, $id) {
        //chiamata a database con where id=$id
        //SELECT * FROM users WHERE id=1
        $book = Book::findOrFail($id);
        return $book;
    }

    public function update(Request $request, $id) {

        $validator = validator::make($request->all(),[
            'title' => 'required|max:200',
            'abstract' => 'required|max:1000',
            'description' => 'required',
            'ean' => 'required|max:13|digits:13',
            'pages' => 'required|integer|max:32767',
            'author_id' => 'required|integer|exists:authors,id'
            // andrebbe inserito il controllo di esistenza dell'author id
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()    
            ], 400);
        };

        $book = Book::findOrFail($id);
        $book->title = $request->input('title');
        $book->abstract = $request->input('abstract');
        $book->description = $request->input('description');
        $book->ean= $request->input('ean');
        $book->pages = $request->input('pages');
        // aggiungere nell'update sia il validator, che l'author_id
        $book->author_id = $request->input('author_id');
        try {
            $book->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
            // con il validator sull'esistenza dell'id non sarebbe necessario 
        }
        
        return $book;
    }

    public function delete(Request $request, $id) {
        $book = Book::findOrFail($id);
        $book->delete();
        return response()->json(null, 204);
    }

    public function patch(Request $request, $id) {
        $validator = validator::make($request->all(),[
            'title' => 'max:200',
            'abstract' => 'max:1000',
            'description' => 'max:1000',
            'ean' => 'max:13|digits:13',
            'pages' => 'integer|max:32767',
            'author_id' => 'integer|exists:authors,id'
            // andrebbe inserito il controllo di esistenza dell'author id
        ]);
        $book = Book::findOrFail($id);
        if ($request->input('title')) {
            $book->title = $request->input('title');
        }
        if ($request->input('abstract')) {
            $book->abstract = $request->input('abstract');
        }
        if ($request->input('description')) {
            $book->description = $request->input('description');
        }
        if ($request->input('ean')) {
            $book->ean= $request->input('ean');
        }
        if ($request->input('pages')) {
            $book->pages = $request->input('pages');
        }
        if ($request->input('author_id')) {
            $book->author_id = $request->input('author_id');
        }
        try {
            $book->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
        return $book;
    }
}
