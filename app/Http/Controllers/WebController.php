<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class WebController extends Controller
{
    public function home() {
        // la funzione home viene utilizzata nel file web.php contenuto dentro routes
        $books = Book::get();
        // facciamo la funzione get() sulla classe Book
        return view('home',
        ['books' => $books]);
    }
}
