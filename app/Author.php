<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    //
    public function books() {
        // specifichiamo che ogni autore ha più libri! Relazione diretta
        return $this->hasMany('App\Book');
    }
}
