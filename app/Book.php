<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function author() {
        // Specificare che ad ogni libro corrisponde un solo autore
        return $this->belongsTo('App\Author');
        // utilizzando i nomi standard tutte le varie relazioni vengono gestite autonomamente da laravel
    }
}
